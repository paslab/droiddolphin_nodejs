$(document).ready(function(){
  update_board();
  setInterval(update_board,10000);
});

function update_board(){
  $.get('/records',function(records){
    var out = '<thead>'+
    '<tr>'+
    '<th>#</th>'+
    '<th>Status</th>'+
    '<th>Classification</th>'+
    '<th>Name</th>'+
    '<th>Description</th>'+
    '<th>Source</th>'+
    '<th>Malware?</th>'+
    '<th>Upload Date</th>'+
    '</tr>'+
    '</thead>'+
    '<tbody>';
  for(var i=0;i<records.length;i++){
    var d = new Date();
    var c = records[i].classification;
    d.setTime(records[i].upload_date);
    var dformat = [ d.getDate().padLeft(),
    (d.getMonth()+1).padLeft(),
    d.getFullYear()].join('/')+
      ' ' +
      [ d.getHours().padLeft(),
    d.getMinutes().padLeft(),
    d.getSeconds().padLeft()].join(':');
    out+='<tr>';
    out+='<td>'+(i+1)+'</td>';
    out+='<td>'+records[i].status+'</td>';
    if(c == '0'){
      out+='<td>Benign</td>';
    }else if(c == '1'){
      out+='<td>Malicious</td>';
    }else{
      out+='<td>'+c+'</td>';
    }
    out+='<td><p class="minimize">'+records[i].name+'</p></td>';
    out+='<td>'+records[i].description+'</td>';
    out+='<td><p class="minimize">'+records[i].source+'</p></td>';
    out+='<td>'+records[i].is_malware+'</td>';
    out+='<td>'+dformat+'</td>';
    out+='</tr>';
  }
  out+='</tbody>';
  $('#board').html(out);
  min_text();
  });
}

function min_text(){
  var minimized_elements = $('p.minimize');

  var num = 20;
  minimized_elements.each(function(){    
    var t = $(this).text();        
    if(t.length < num) return;

    $(this).html(
      t.slice(0,num)+'<span>... </span><a href="#" class="more pure-button pure-button-primary button-xsmall">More</a>'+
      '<span style="display:none;">'+ t.slice(num,t.length)+' <a href="#" class="less pure-button pure-button-primary button-xsmall">Less</a></span>'
      );

  }); 

  $('a.more', minimized_elements).click(function(event){
    event.preventDefault();
    $(this).hide().prev().hide();
    $(this).next().show();        
  });

  $('a.less', minimized_elements).click(function(event){
    event.preventDefault();
    $(this).parent().hide().prev().show().prev().show();    
  });
}


Number.prototype.padLeft = function(base,chr){
  var  len = (String(base || 10).length - String(this).length)+1;
  return len > 0? new Array(len).join(chr || '0')+this : this;
}
