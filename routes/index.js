
/*
 * GET home page.
 */

var fs = require('fs');

exports.index = function(req, res){
  res.render('index', { title: 'DroidDolphin' });
};

exports.upload = function(req, res){
  if(req.files.apk_file.size == 0 || req.files.apk_file.name.search(".apk") == -1){
    res.redirect('/');
  }
  fs.readFile(req.files.apk_file.path, function (err, data) {
    var date = new Date();
    var name = req.files.apk_file.name;
    var des = req.body.description;
    var src = req.body.source;
    var is_malware = req.body.is_malware;
    var newPath = __dirname + "/../uploads/"+date.getTime()+".apk";
    fs.writeFile(newPath, data, function (err) {
      var record = new mongoose_util.Record({upload_date: date.getTime(), name: name, description: des, source: src, is_malware: is_malware});
      mongoose_util.save(record,function(){
        res.redirect('/');
      });
    });
  });
};

exports.records = function(req,res){
  mongoose_util.Record.find({}).sort({upload_date:'desc'}).exec(function(err,records){
    res.send(records);
  });
};
