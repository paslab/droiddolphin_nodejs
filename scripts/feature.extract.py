#!/usr/bin/python2
# feature.extract.py: Extract feature from the log file created by APE_BOX, the output format is for SVM
# written by Wen-Chieh Wu, 2014

# Usage:
#  python2 feature.extract.py -l <label> -f <log_file_name>
#
#  label:         SVM label, representing which cluster it is
#  log_file_name: The input log file name
#
# Example:
#  python2 feature.extract.py 0 benign.log
#
# The result of the SVM feature will be output to stdout

import sys
import string

label = sys.argv[1]
log_name = sys.argv[2]

monkey_flag = False
dex_flag = False

purify_log = []

tag_list = [
    "DexClassLoader",
    "ServiceStart",
    "RecvNet",
    "FdAccess",
    "file read",
    "file write",
    "OpenNet",
    "CloseNet",
    "SendNet",
    "DataLeak",
    "SendSMS",
    "PhoneCall",
    "CryptoUsage",
    #API part
    "Landroid/content/Intent",
    "Landroid/content/ContextWrapper",
    "Landroid/net/Uri",
    "Ljava/io/FileReader",
    "Ljava/io/FileWriter",
    "Ljava/net/URL",
    "Ljava/io/BufferedReader",
    "Ljava/io/BufferedWriter",
    "Ljava/io/InputStreamReader",
    "Ljava/io/OutputStreamWriter",
    "Ljava/io/CharArrayReader",
    "Ljava/io/CharArrayWriter",
    "Ljava/io/FilterReader",
    "Ljava/io/FilterWriter",
    "Ljava/io/StringReader",
    "Ljava/io/StringWriter",
    "Ljava/io/PrintWriter",
    "Landroid/database/sqlite/SQLiteDatabase",
    "Landroid/content/Content/Resolver",
    "Landroid/telephony/SmsManager",
    "Landroid/content/pm/Packagemanager",
    "Landroid/telephony/TelephonyManager",
    "Ljava/security/MessageDigest",
    "Ljavax/crypto/Cipher",
    "Ljavax/crypto/spec/SecretKeySpec"
    ]

ntag = len(tag_list)

for line in open(log_name,'r'):
  if string.find(line,'monkey.jar') != -1:
    monkey_flag = True
  elif string.find(line,'DexClassLoader') != -1 and monkey_flag == True:
    dex_flag = True
  if monkey_flag == True and dex_flag == True:
    purify_log.append(line)

purify_log.append('NoOp');
purify_log.append('NoOp');


total = 0

#1-gram
_1_gram = []
for i in range(0,ntag):
  _1_gram.append(0)

#2-gram
_2_gram = []
for i in range(0,ntag*ntag):
  _2_gram.append(0)

#3-gram
_3_gram = []
for i in range(0,ntag*ntag*ntag):
  _3_gram.append(0)

for i in range(0,len(purify_log)):
  for j in range(0,ntag):
    if string.find(purify_log[i],tag_list[j]) != -1:
      _1_gram[j] += 1
      total+=1
      for k in range(0,ntag):
        if string.find(purify_log[i],tag_list[j]) != -1 and string.find(purify_log[i+1],tag_list[k]) != -1:
          _2_gram[j*ntag+k] += 1
          total+=1
          for l in range(0,ntag):
            if string.find(purify_log[i],tag_list[j]) != -1 and string.find(purify_log[i+1],tag_list[k]) != -1 and string.find(purify_log[i+2],tag_list[l]) != -1:
              _3_gram[j*ntag*ntag+k*ntag+l] += 1
              total+=1


#output
#total=1 # comment this line to get the normalized result
output = label + ' '
cnt = 1
for i in range(0,len(_1_gram)):
  if _1_gram[i] != 0:
    output = output + str(cnt) + ':' + str(float(_1_gram[i])/total) + ' '
  cnt = cnt + 1

for i in range(0,len(_2_gram)):
  if _2_gram[i] != 0:
    output = output + str(cnt) + ':' + str(float(_2_gram[i])/total) + ' '
  cnt = cnt + 1

for i in range(0,len(_3_gram)):
  if _3_gram[i] != 0:
    output = output + str(cnt) + ':' + str(float(_3_gram[i])/total) + ' '
  cnt = cnt + 1

if len(output) != 2:
  print output
