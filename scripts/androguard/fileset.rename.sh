#!/bin/bash

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")
for apk in `find $1 -type f -name "*.apk"`
do
  SHA1=`sha1sum $apk | cut -d ' ' -f1`
  echo $SHA1.apk
  cp $apk $2/$SHA1.apk
done
IFS=$SAVEIFS
