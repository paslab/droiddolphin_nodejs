#!/usr/bin/env python2

import sys
import os
import pprint
import traceback

from optparse import OptionParser

from androguard.core import androconf
from androguard.core.bytecodes import apk
from androguard.core.bytecodes import dvm
from androguard.core.analysis import analysis

intent_attrs = ["action", "category", "extras"]

option_0 = { 'name' : ('-i', '--input'), 'help' : 'file : use this filename (APK)', 'nargs' : 1}

options = [option_0]

def main(options, arguments):
  if options.input != None:
    ret_type = androconf.is_android(options.input)
    #print os.path.basename(options.input)
    if ret_type == "APK":
      try:
        a = apk.APK(options.input, zipmodule=2)
        if a.is_valid_APK():
          print a.get_package()
        else:
          print "INVALID"
      except Exception, e:
        print "ERROR", e
        traceback.print_exc()

if __name__ == "__main__" :
  parser = OptionParser()
  for option in options:
    param = option['name']
    del option['name']
    parser.add_option(*param, **option)

  options, arguments = parser.parse_args()
  sys.argv[:] = arguments
  main(options, arguments)
