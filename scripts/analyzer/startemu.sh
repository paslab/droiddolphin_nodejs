#!/usr/bin/env bash

base_dir=$(dirname $0)
image_dir=${base_dir}/images

if [ "$1" == "" ]; then
    echo "Please give the avd name!"
    exit 0;
fi

emulator-arm -avd $1 -system ${image_dir}/system$1.img -ramdisk ${image_dir}/ramdisk$1.img -wipe-data -prop dalvik.vm.execution-mode=int:portable -noaudio -no-boot-anim &
