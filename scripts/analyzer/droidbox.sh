#!/bin/bash

if [ ! -f "$1.log" ]
then
  adb -s $2 install $1
  python2 "scripts/droidbox.py" $1 $2 10
  adb -s $2 uninstall `python2 scripts/andropkgname.py -i $1`
else
  echo "$1.log exists, Skip..."
fi
