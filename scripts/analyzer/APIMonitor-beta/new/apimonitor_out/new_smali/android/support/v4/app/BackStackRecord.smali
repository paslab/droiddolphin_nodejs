.class final Landroid/support/v4/app/BackStackRecord;
.super Landroid/support/v4/app/FragmentTransaction;
.source "BackStackRecord.java"
.implements Landroid/support/v4/app/FragmentManager$BackStackEntry;
.implements Ljava/lang/Runnable;
.field static final OP_ADD:I = 0x1
.field static final OP_ATTACH:I = 0x7
.field static final OP_DETACH:I = 0x6
.field static final OP_HIDE:I = 0x4
.field static final OP_NULL:I = 0x0
.field static final OP_REMOVE:I = 0x3
.field static final OP_REPLACE:I = 0x2
.field static final OP_SHOW:I = 0x5
.field static final TAG:Ljava/lang/String; = "BackStackEntry"
.field  mAddToBackStack:Z
.field  mAllowAddToBackStack:Z
.field  mBreadCrumbShortTitleRes:I
.field  mBreadCrumbShortTitleText:Ljava/lang/CharSequence;
.field  mBreadCrumbTitleRes:I
.field  mBreadCrumbTitleText:Ljava/lang/CharSequence;
.field  mCommitted:Z
.field  mEnterAnim:I
.field  mExitAnim:I
.field  mHead:Landroid/support/v4/app/BackStackRecord$Op;
.field  mIndex:I
.field final mManager:Landroid/support/v4/app/FragmentManagerImpl;
.field  mName:Ljava/lang/String;
.field  mNumOp:I
.field  mPopEnterAnim:I
.field  mPopExitAnim:I
.field  mTail:Landroid/support/v4/app/BackStackRecord$Op;
.field  mTransition:I
.field  mTransitionStyle:I
.method public constructor <init>(Landroid/support/v4/app/FragmentManagerImpl;)V
.registers 3
invoke-direct {p0}, Landroid/support/v4/app/FragmentTransaction;-><init>()V
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mAllowAddToBackStack:Z
iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
return-void
.end method
.method private doAddOp(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
.registers 9
iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iput-object v1, p2, Landroid/support/v4/app/Fragment;->mFragmentManager:Landroid/support/v4/app/FragmentManagerImpl;
if-eqz p3, :cond_43
iget-object v1, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;
if-eqz v1, :cond_41
iget-object v1, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;
invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
move-result v1
if-nez v1, :cond_41
new-instance v1, Ljava/lang/IllegalStateException;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Can\'t change tag of fragment "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, ": was "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget-object v3, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, " now "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v1
:cond_41
iput-object p3, p2, Landroid/support/v4/app/Fragment;->mTag:Ljava/lang/String;
:cond_43
if-eqz p1, :cond_80
iget v1, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I
if-eqz v1, :cond_7c
iget v1, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I
if-eq v1, p1, :cond_7c
new-instance v1, Ljava/lang/IllegalStateException;
new-instance v2, Ljava/lang/StringBuilder;
invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V
const-string v3, "Can\'t change container ID of fragment "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, ": was "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
iget v3, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v2
const-string v3, " now "
invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v2
invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v2
invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v1
:cond_7c
iput p1, p2, Landroid/support/v4/app/Fragment;->mFragmentId:I
iput p1, p2, Landroid/support/v4/app/Fragment;->mContainerId:I
:cond_80
new-instance v0, Landroid/support/v4/app/BackStackRecord$Op;
invoke-direct {v0}, Landroid/support/v4/app/BackStackRecord$Op;-><init>()V
iput p4, v0, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
iput-object p2, v0, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->addOp(Landroid/support/v4/app/BackStackRecord$Op;)V
return-void
.end method
.method public add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
.registers 5
const/4 v0, 0x0
const/4 v1, 0x1
invoke-direct {p0, p1, p2, v0, v1}, Landroid/support/v4/app/BackStackRecord;->doAddOp(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
return-object p0
.end method
.method public add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
.registers 5
const/4 v0, 0x1
invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/BackStackRecord;->doAddOp(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
return-object p0
.end method
.method public add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
.registers 5
const/4 v0, 0x0
const/4 v1, 0x1
invoke-direct {p0, v0, p1, p2, v1}, Landroid/support/v4/app/BackStackRecord;->doAddOp(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
return-object p0
.end method
.method  addOp(Landroid/support/v4/app/BackStackRecord$Op;)V
.registers 3
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mHead:Landroid/support/v4/app/BackStackRecord$Op;
if-nez v0, :cond_1f
iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->mTail:Landroid/support/v4/app/BackStackRecord$Op;
iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->mHead:Landroid/support/v4/app/BackStackRecord$Op;
:goto_8
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mEnterAnim:I
iput v0, p1, Landroid/support/v4/app/BackStackRecord$Op;->enterAnim:I
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mExitAnim:I
iput v0, p1, Landroid/support/v4/app/BackStackRecord$Op;->exitAnim:I
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mPopEnterAnim:I
iput v0, p1, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mPopExitAnim:I
iput v0, p1, Landroid/support/v4/app/BackStackRecord$Op;->popExitAnim:I
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mNumOp:I
add-int/lit8 v0, v0, 0x1
iput v0, p0, Landroid/support/v4/app/BackStackRecord;->mNumOp:I
return-void
:cond_1f
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mTail:Landroid/support/v4/app/BackStackRecord$Op;
iput-object v0, p1, Landroid/support/v4/app/BackStackRecord$Op;->prev:Landroid/support/v4/app/BackStackRecord$Op;
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mTail:Landroid/support/v4/app/BackStackRecord$Op;
iput-object p1, v0, Landroid/support/v4/app/BackStackRecord$Op;->next:Landroid/support/v4/app/BackStackRecord$Op;
iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->mTail:Landroid/support/v4/app/BackStackRecord$Op;
goto :goto_8
.end method
.method public addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
.registers 4
iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mAllowAddToBackStack:Z
if-nez v0, :cond_c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "This FragmentTransaction is not allowed to be added to the back stack."
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mAddToBackStack:Z
iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->mName:Ljava/lang/String;
return-object p0
.end method
.method public attach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
.registers 4
new-instance v0, Landroid/support/v4/app/BackStackRecord$Op;
invoke-direct {v0}, Landroid/support/v4/app/BackStackRecord$Op;-><init>()V
const/4 v1, 0x7
iput v1, v0, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
iput-object p1, v0, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->addOp(Landroid/support/v4/app/BackStackRecord$Op;)V
return-object p0
.end method
.method  bumpBackStackNesting(I)V
.registers 8
iget-boolean v3, p0, Landroid/support/v4/app/BackStackRecord;->mAddToBackStack:Z
if-nez v3, :cond_5
:cond_4
return-void
:cond_5
sget-boolean v3, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v3, :cond_2b
const-string v3, "BackStackEntry"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "Bump nesting in "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, " by "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_2b
iget-object v1, p0, Landroid/support/v4/app/BackStackRecord;->mHead:Landroid/support/v4/app/BackStackRecord$Op;
:goto_2d
if-eqz v1, :cond_4
iget-object v3, v1, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
if-eqz v3, :cond_66
iget-object v3, v1, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
add-int/2addr v4, p1
iput v4, v3, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
sget-boolean v3, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v3, :cond_66
const-string v3, "BackStackEntry"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "Bump nesting of "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
iget-object v5, v1, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, " to "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
iget-object v5, v1, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v5, v5, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_66
iget-object v3, v1, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
if-eqz v3, :cond_ac
iget-object v3, v1, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v3}, Ljava/util/ArrayList;->size()I
move-result v3
add-int/lit8 v0, v3, -0x1
:goto_72
if-ltz v0, :cond_ac
iget-object v3, v1, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v2
check-cast v2, Landroid/support/v4/app/Fragment;
iget v3, v2, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
add-int/2addr v3, p1
iput v3, v2, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
sget-boolean v3, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v3, :cond_a9
const-string v3, "BackStackEntry"
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
const-string v5, "Bump nesting of "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, " to "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
iget v5, v2, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v4
invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_a9
add-int/lit8 v0, v0, -0x1
goto :goto_72
:cond_ac
iget-object v1, v1, Landroid/support/v4/app/BackStackRecord$Op;->next:Landroid/support/v4/app/BackStackRecord$Op;
goto/16 :goto_2d
.end method
.method public commit()I
.registers 2
const/4 v0, 0x0
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->commitInternal(Z)I
move-result v0
return v0
.end method
.method public commitAllowingStateLoss()I
.registers 2
const/4 v0, 0x1
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->commitInternal(Z)I
move-result v0
return v0
.end method
.method  commitInternal(Z)I
.registers 5
iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mCommitted:Z
if-eqz v0, :cond_c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "commit already called"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
sget-boolean v0, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v0, :cond_28
const-string v0, "BackStackEntry"
new-instance v1, Ljava/lang/StringBuilder;
invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
const-string v2, "Commit: "
invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v1
invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_28
const/4 v0, 0x1
iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mCommitted:Z
iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mAddToBackStack:Z
if-eqz v0, :cond_3f
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
invoke-virtual {v0, p0}, Landroid/support/v4/app/FragmentManagerImpl;->allocBackStackIndex(Landroid/support/v4/app/BackStackRecord;)I
move-result v0
iput v0, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
:goto_37
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
invoke-virtual {v0, p0, p1}, Landroid/support/v4/app/FragmentManagerImpl;->enqueueAction(Ljava/lang/Runnable;Z)V
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
return v0
:cond_3f
const/4 v0, -0x1
iput v0, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
goto :goto_37
.end method
.method public detach(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
.registers 4
new-instance v0, Landroid/support/v4/app/BackStackRecord$Op;
invoke-direct {v0}, Landroid/support/v4/app/BackStackRecord$Op;-><init>()V
const/4 v1, 0x6
iput v1, v0, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
iput-object p1, v0, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->addOp(Landroid/support/v4/app/BackStackRecord$Op;)V
return-object p0
.end method
.method public disallowAddToBackStack()Landroid/support/v4/app/FragmentTransaction;
.registers 3
iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mAddToBackStack:Z
if-eqz v0, :cond_c
new-instance v0, Ljava/lang/IllegalStateException;
const-string v1, "This transaction is already being added to the back stack"
invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v0
:cond_c
const/4 v0, 0x0
iput-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mAllowAddToBackStack:Z
return-object p0
.end method
.method public dump(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
.registers 11
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "mName="
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mName:Ljava/lang/String;
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " mIndex="
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;I)V
const-string v4, " mCommitted="
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget-boolean v4, p0, Landroid/support/v4/app/BackStackRecord;->mCommitted:Z
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Z)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
if-eqz v4, :cond_44
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "mTransition=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " mTransitionStyle=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
:cond_44
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mEnterAnim:I
if-nez v4, :cond_4c
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mExitAnim:I
if-eqz v4, :cond_6b
:cond_4c
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "mEnterAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mEnterAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " mExitAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mExitAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
:cond_6b
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mPopEnterAnim:I
if-nez v4, :cond_73
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mPopExitAnim:I
if-eqz v4, :cond_92
:cond_73
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "mPopEnterAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mPopEnterAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " mPopExitAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mPopExitAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
:cond_92
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleRes:I
if-nez v4, :cond_9a
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;
if-eqz v4, :cond_b5
:cond_9a
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "mBreadCrumbTitleRes=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleRes:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " mBreadCrumbTitleText="
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/Object;)V
:cond_b5
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleRes:I
if-nez v4, :cond_bd
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;
if-eqz v4, :cond_d8
:cond_bd
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "mBreadCrumbShortTitleRes=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleRes:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " mBreadCrumbShortTitleText="
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/Object;)V
:cond_d8
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mHead:Landroid/support/v4/app/BackStackRecord$Op;
if-eqz v4, :cond_1bd
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "Operations:"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
new-instance v4, Ljava/lang/StringBuilder;
invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V
invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
const-string v5, "    "
invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v4
invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v1
iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->mHead:Landroid/support/v4/app/BackStackRecord$Op;
const/4 v2, 0x0
:goto_fa
if-eqz v3, :cond_1bd
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "  Op #"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
invoke-static {p3, v2}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;I)V
const-string v4, ":"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
invoke-static {p3, v1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "cmd="
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;I)V
const-string v4, " fragment="
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/Object;)V
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->enterAnim:I
if-nez v4, :cond_12b
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->exitAnim:I
if-eqz v4, :cond_14a
:cond_12b
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "enterAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->enterAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " exitAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->exitAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
:cond_14a
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
if-nez v4, :cond_152
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popExitAnim:I
if-eqz v4, :cond_171
:cond_152
invoke-static {p3, p1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "popEnterAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, " popExitAnim=#"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popExitAnim:I
invoke-static {v4}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
:cond_171
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
if-eqz v4, :cond_1b9
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
move-result v4
if-lez v4, :cond_1b9
const/4 v0, 0x0
:goto_17e
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
move-result v4
if-ge v0, v4, :cond_1b9
invoke-static {p3, v1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
move-result v4
const/4 v5, 0x1
if-ne v4, v5, :cond_1a3
const-string v4, "Removed: "
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
:goto_197
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v4
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/Object;)V
add-int/lit8 v0, v0, 0x1
goto :goto_17e
:cond_1a3
const-string v4, "Removed:"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->println(Ljava/io/PrintWriter;Ljava/lang/String;)V
invoke-static {p3, v1}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
const-string v4, "  #"
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
invoke-static {p3, v2}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;I)V
const-string v4, ": "
invoke-static {p3, v4}, Ldroidbox/java/io/PrintWriter;->print(Ljava/io/PrintWriter;Ljava/lang/String;)V
goto :goto_197
:cond_1b9
iget-object v3, v3, Landroid/support/v4/app/BackStackRecord$Op;->next:Landroid/support/v4/app/BackStackRecord$Op;
goto/16 :goto_fa
:cond_1bd
return-void
.end method
.method public getBreadCrumbShortTitle()Ljava/lang/CharSequence;
.registers 3
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleRes:I
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget-object v0, v0, Landroid/support/v4/app/FragmentManagerImpl;->mActivity:Landroid/support/v4/app/FragmentActivity;
iget v1, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleRes:I
invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getText(I)Ljava/lang/CharSequence;
move-result-object v0
:goto_e
return-object v0
:cond_f
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;
goto :goto_e
.end method
.method public getBreadCrumbShortTitleRes()I
.registers 2
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleRes:I
return v0
.end method
.method public getBreadCrumbTitle()Ljava/lang/CharSequence;
.registers 3
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleRes:I
if-eqz v0, :cond_f
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget-object v0, v0, Landroid/support/v4/app/FragmentManagerImpl;->mActivity:Landroid/support/v4/app/FragmentActivity;
iget v1, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleRes:I
invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getText(I)Ljava/lang/CharSequence;
move-result-object v0
:goto_e
return-object v0
:cond_f
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;
goto :goto_e
.end method
.method public getBreadCrumbTitleRes()I
.registers 2
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleRes:I
return v0
.end method
.method public getId()I
.registers 2
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
return v0
.end method
.method public getName()Ljava/lang/String;
.registers 2
iget-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mName:Ljava/lang/String;
return-object v0
.end method
.method public getTransition()I
.registers 2
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
return v0
.end method
.method public getTransitionStyle()I
.registers 2
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
return v0
.end method
.method public hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
.registers 4
new-instance v0, Landroid/support/v4/app/BackStackRecord$Op;
invoke-direct {v0}, Landroid/support/v4/app/BackStackRecord$Op;-><init>()V
const/4 v1, 0x4
iput v1, v0, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
iput-object p1, v0, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->addOp(Landroid/support/v4/app/BackStackRecord$Op;)V
return-object p0
.end method
.method public isAddToBackStackAllowed()Z
.registers 2
iget-boolean v0, p0, Landroid/support/v4/app/BackStackRecord;->mAllowAddToBackStack:Z
return v0
.end method
.method public isEmpty()Z
.registers 2
iget v0, p0, Landroid/support/v4/app/BackStackRecord;->mNumOp:I
if-nez v0, :cond_6
const/4 v0, 0x1
:goto_5
return v0
:cond_6
const/4 v0, 0x0
goto :goto_5
.end method
.method public popFromBackStack(Z)V
.registers 12
const/4 v7, 0x0
const/4 v9, -0x1
sget-boolean v4, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v4, :cond_1e
const-string v4, "BackStackEntry"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v6, "popFromBackStack: "
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_1e
invoke-virtual {p0, v9}, Landroid/support/v4/app/BackStackRecord;->bumpBackStackNesting(I)V
iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->mTail:Landroid/support/v4/app/BackStackRecord$Op;
:goto_23
if-eqz v3, :cond_ee
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
packed-switch v4, :pswitch_data_110
new-instance v4, Ljava/lang/IllegalArgumentException;
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v6, "Unknown cmd: "
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
iget v6, v3, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v4
:pswitch_45
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popExitAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v5}, Landroid/support/v4/app/FragmentManagerImpl;->reverseTransit(I)I
move-result v5
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->removeFragment(Landroid/support/v4/app/Fragment;II)V
:cond_58
:goto_58
iget-object v3, v3, Landroid/support/v4/app/BackStackRecord$Op;->prev:Landroid/support/v4/app/BackStackRecord$Op;
goto :goto_23
:pswitch_5b
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
if-eqz v0, :cond_70
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popExitAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v5}, Landroid/support/v4/app/FragmentManagerImpl;->reverseTransit(I)I
move-result v5
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->removeFragment(Landroid/support/v4/app/Fragment;II)V
:cond_70
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
if-eqz v4, :cond_58
const/4 v1, 0x0
:goto_75
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
move-result v4
if-ge v1, v4, :cond_58
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v2
check-cast v2, Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
iput v4, v2, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
invoke-virtual {v4, v2, v7}, Landroid/support/v4/app/FragmentManagerImpl;->addFragment(Landroid/support/v4/app/Fragment;Z)V
add-int/lit8 v1, v1, 0x1
goto :goto_75
:pswitch_91
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
invoke-virtual {v4, v0, v7}, Landroid/support/v4/app/FragmentManagerImpl;->addFragment(Landroid/support/v4/app/Fragment;Z)V
goto :goto_58
:pswitch_9d
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v5}, Landroid/support/v4/app/FragmentManagerImpl;->reverseTransit(I)I
move-result v5
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->showFragment(Landroid/support/v4/app/Fragment;II)V
goto :goto_58
:pswitch_b1
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popExitAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v5}, Landroid/support/v4/app/FragmentManagerImpl;->reverseTransit(I)I
move-result v5
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->hideFragment(Landroid/support/v4/app/Fragment;II)V
goto :goto_58
:pswitch_c5
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v5}, Landroid/support/v4/app/FragmentManagerImpl;->reverseTransit(I)I
move-result v5
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->attachFragment(Landroid/support/v4/app/Fragment;II)V
goto :goto_58
:pswitch_d9
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->popEnterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v5}, Landroid/support/v4/app/FragmentManagerImpl;->reverseTransit(I)I
move-result v5
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->detachFragment(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_58
:cond_ee
if-eqz p1, :cond_102
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget-object v5, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, v5, Landroid/support/v4/app/FragmentManagerImpl;->mCurState:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
invoke-static {v6}, Landroid/support/v4/app/FragmentManagerImpl;->reverseTransit(I)I
move-result v6
iget v7, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
const/4 v8, 0x1
invoke-virtual {v4, v5, v6, v7, v8}, Landroid/support/v4/app/FragmentManagerImpl;->moveToState(IIIZ)V
:cond_102
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
if-ltz v4, :cond_10f
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
invoke-virtual {v4, v5}, Landroid/support/v4/app/FragmentManagerImpl;->freeBackStackIndex(I)V
iput v9, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
:cond_10f
return-void
:pswitch_data_110
.packed-switch 0x1
:pswitch_45
:pswitch_5b
:pswitch_91
:pswitch_9d
:pswitch_b1
:pswitch_c5
:pswitch_d9
.end packed-switch
.end method
.method public remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
.registers 4
new-instance v0, Landroid/support/v4/app/BackStackRecord$Op;
invoke-direct {v0}, Landroid/support/v4/app/BackStackRecord$Op;-><init>()V
const/4 v1, 0x3
iput v1, v0, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
iput-object p1, v0, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->addOp(Landroid/support/v4/app/BackStackRecord$Op;)V
return-object p0
.end method
.method public replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0}, Landroid/support/v4/app/BackStackRecord;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
move-result-object v0
return-object v0
.end method
.method public replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;
.registers 6
if-nez p1, :cond_a
new-instance v0, Ljava/lang/IllegalArgumentException;
const-string v1, "Must use non-zero containerViewId"
invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v0
:cond_a
const/4 v0, 0x2
invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/BackStackRecord;->doAddOp(ILandroid/support/v4/app/Fragment;Ljava/lang/String;I)V
return-object p0
.end method
.method public run()V
.registers 10
const/4 v8, 0x1
const/4 v7, 0x0
sget-boolean v4, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v4, :cond_1e
const-string v4, "BackStackEntry"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v6, "Run: "
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_1e
iget-boolean v4, p0, Landroid/support/v4/app/BackStackRecord;->mAddToBackStack:Z
if-eqz v4, :cond_2e
iget v4, p0, Landroid/support/v4/app/BackStackRecord;->mIndex:I
if-gez v4, :cond_2e
new-instance v4, Ljava/lang/IllegalStateException;
const-string v5, "addToBackStack() called after commit()"
invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V
throw v4
:cond_2e
invoke-virtual {p0, v8}, Landroid/support/v4/app/BackStackRecord;->bumpBackStackNesting(I)V
iget-object v3, p0, Landroid/support/v4/app/BackStackRecord;->mHead:Landroid/support/v4/app/BackStackRecord$Op;
:goto_33
if-eqz v3, :cond_168
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
packed-switch v4, :pswitch_data_180
new-instance v4, Ljava/lang/IllegalArgumentException;
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v6, "Unknown cmd: "
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
iget v6, v3, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V
throw v4
:pswitch_55
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->enterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
invoke-virtual {v4, v0, v7}, Landroid/support/v4/app/FragmentManagerImpl;->addFragment(Landroid/support/v4/app/Fragment;Z)V
:cond_60
:goto_60
iget-object v3, v3, Landroid/support/v4/app/BackStackRecord$Op;->next:Landroid/support/v4/app/BackStackRecord$Op;
goto :goto_33
:pswitch_63
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget-object v4, v4, Landroid/support/v4/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;
if-eqz v4, :cond_106
const/4 v1, 0x0
:goto_6c
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget-object v4, v4, Landroid/support/v4/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;
invoke-virtual {v4}, Ljava/util/ArrayList;->size()I
move-result v4
if-ge v1, v4, :cond_106
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget-object v4, v4, Landroid/support/v4/app/FragmentManagerImpl;->mAdded:Ljava/util/ArrayList;
invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;
move-result-object v2
check-cast v2, Landroid/support/v4/app/Fragment;
sget-boolean v4, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v4, :cond_a6
const-string v4, "BackStackEntry"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v6, "OP_REPLACE: adding="
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v5
const-string v6, " old="
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_a6
if-eqz v0, :cond_ae
iget v4, v2, Landroid/support/v4/app/Fragment;->mContainerId:I
iget v5, v0, Landroid/support/v4/app/Fragment;->mContainerId:I
if-ne v4, v5, :cond_b3
:cond_ae
if-ne v2, v0, :cond_b6
const/4 v0, 0x0
iput-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
:goto_b3
:cond_b3
add-int/lit8 v1, v1, 0x1
goto :goto_6c
:cond_b6
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
if-nez v4, :cond_c1
new-instance v4, Ljava/util/ArrayList;
invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V
iput-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
:cond_c1
iget-object v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->removed:Ljava/util/ArrayList;
invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->exitAnim:I
iput v4, v2, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-boolean v4, p0, Landroid/support/v4/app/BackStackRecord;->mAddToBackStack:Z
if-eqz v4, :cond_fc
iget v4, v2, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
add-int/lit8 v4, v4, 0x1
iput v4, v2, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
sget-boolean v4, Landroid/support/v4/app/FragmentManagerImpl;->DEBUG:Z
if-eqz v4, :cond_fc
const-string v4, "BackStackEntry"
new-instance v5, Ljava/lang/StringBuilder;
invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V
const-string v6, "Bump nesting of "
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
move-result-object v5
const-string v6, " to "
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
iget v6, v2, Landroid/support/v4/app/Fragment;->mBackStackNesting:I
invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:cond_fc
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v2, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->removeFragment(Landroid/support/v4/app/Fragment;II)V
goto :goto_b3
:cond_106
if-eqz v0, :cond_60
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->enterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
invoke-virtual {v4, v0, v7}, Landroid/support/v4/app/FragmentManagerImpl;->addFragment(Landroid/support/v4/app/Fragment;Z)V
goto/16 :goto_60
:pswitch_113
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->exitAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->removeFragment(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_60
:pswitch_124
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->exitAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->hideFragment(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_60
:pswitch_135
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->enterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->showFragment(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_60
:pswitch_146
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->exitAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->detachFragment(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_60
:pswitch_157
iget-object v0, v3, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
iget v4, v3, Landroid/support/v4/app/BackStackRecord$Op;->enterAnim:I
iput v4, v0, Landroid/support/v4/app/Fragment;->mNextAnim:I
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v0, v5, v6}, Landroid/support/v4/app/FragmentManagerImpl;->attachFragment(Landroid/support/v4/app/Fragment;II)V
goto/16 :goto_60
:cond_168
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget-object v5, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
iget v5, v5, Landroid/support/v4/app/FragmentManagerImpl;->mCurState:I
iget v6, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
iget v7, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
invoke-virtual {v4, v5, v6, v7, v8}, Landroid/support/v4/app/FragmentManagerImpl;->moveToState(IIIZ)V
iget-boolean v4, p0, Landroid/support/v4/app/BackStackRecord;->mAddToBackStack:Z
if-eqz v4, :cond_17e
iget-object v4, p0, Landroid/support/v4/app/BackStackRecord;->mManager:Landroid/support/v4/app/FragmentManagerImpl;
invoke-virtual {v4, p0}, Landroid/support/v4/app/FragmentManagerImpl;->addBackStackState(Landroid/support/v4/app/BackStackRecord;)V
:cond_17e
return-void
nop
:pswitch_data_180
.packed-switch 0x1
:pswitch_55
:pswitch_63
:pswitch_113
:pswitch_124
:pswitch_135
:pswitch_146
:pswitch_157
.end packed-switch
.end method
.method public setBreadCrumbShortTitle(I)Landroid/support/v4/app/FragmentTransaction;
.registers 3
iput p1, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleRes:I
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;
return-object p0
.end method
.method public setBreadCrumbShortTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/FragmentTransaction;
.registers 3
const/4 v0, 0x0
iput v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleRes:I
iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbShortTitleText:Ljava/lang/CharSequence;
return-object p0
.end method
.method public setBreadCrumbTitle(I)Landroid/support/v4/app/FragmentTransaction;
.registers 3
iput p1, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleRes:I
const/4 v0, 0x0
iput-object v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;
return-object p0
.end method
.method public setBreadCrumbTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/FragmentTransaction;
.registers 3
const/4 v0, 0x0
iput v0, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleRes:I
iput-object p1, p0, Landroid/support/v4/app/BackStackRecord;->mBreadCrumbTitleText:Ljava/lang/CharSequence;
return-object p0
.end method
.method public setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;
.registers 4
const/4 v0, 0x0
invoke-virtual {p0, p1, p2, v0, v0}, Landroid/support/v4/app/BackStackRecord;->setCustomAnimations(IIII)Landroid/support/v4/app/FragmentTransaction;
move-result-object v0
return-object v0
.end method
.method public setCustomAnimations(IIII)Landroid/support/v4/app/FragmentTransaction;
.registers 5
iput p1, p0, Landroid/support/v4/app/BackStackRecord;->mEnterAnim:I
iput p2, p0, Landroid/support/v4/app/BackStackRecord;->mExitAnim:I
iput p3, p0, Landroid/support/v4/app/BackStackRecord;->mPopEnterAnim:I
iput p4, p0, Landroid/support/v4/app/BackStackRecord;->mPopExitAnim:I
return-object p0
.end method
.method public setTransition(I)Landroid/support/v4/app/FragmentTransaction;
.registers 2
iput p1, p0, Landroid/support/v4/app/BackStackRecord;->mTransition:I
return-object p0
.end method
.method public setTransitionStyle(I)Landroid/support/v4/app/FragmentTransaction;
.registers 2
iput p1, p0, Landroid/support/v4/app/BackStackRecord;->mTransitionStyle:I
return-object p0
.end method
.method public show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;
.registers 4
new-instance v0, Landroid/support/v4/app/BackStackRecord$Op;
invoke-direct {v0}, Landroid/support/v4/app/BackStackRecord$Op;-><init>()V
const/4 v1, 0x5
iput v1, v0, Landroid/support/v4/app/BackStackRecord$Op;->cmd:I
iput-object p1, v0, Landroid/support/v4/app/BackStackRecord$Op;->fragment:Landroid/support/v4/app/Fragment;
invoke-virtual {p0, v0}, Landroid/support/v4/app/BackStackRecord;->addOp(Landroid/support/v4/app/BackStackRecord$Op;)V
return-object p0
.end method