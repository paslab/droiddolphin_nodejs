.class public Lapkil/tests/APKIL;
.super Landroid/app/Activity;
.source "APKIL.java"
.field private static final KEY:[B
.field private static final KEY2:[B
.field private fileContent:Ljava/lang/String;
.field private imei:Ljava/lang/String;
.field private imsi:Ljava/lang/String;
.method static constructor <clinit>()V
.registers 8
const/16 v7, 0x8
const/4 v6, 0x7
const/4 v5, 0x6
const/4 v4, 0x4
const/4 v3, 0x2
const/16 v0, 0x10
new-array v0, v0, [B
const/4 v1, 0x1
const/16 v2, 0x2a
aput-byte v2, v0, v1
aput-byte v3, v0, v3
const/4 v1, 0x3
const/16 v2, 0x36
aput-byte v2, v0, v1
aput-byte v4, v0, v4
const/4 v1, 0x5
const/16 v2, 0x2d
aput-byte v2, v0, v1
aput-byte v5, v0, v5
aput-byte v6, v0, v6
const/16 v1, 0x41
aput-byte v1, v0, v7
const/16 v1, 0x9
const/16 v2, 0x9
aput-byte v2, v0, v1
const/16 v1, 0xa
const/16 v2, 0x36
aput-byte v2, v0, v1
const/16 v1, 0xb
const/16 v2, 0xb
aput-byte v2, v0, v1
const/16 v1, 0xc
const/16 v2, 0xc
aput-byte v2, v0, v1
const/16 v1, 0xd
const/16 v2, 0xd
aput-byte v2, v0, v1
const/16 v1, 0xe
const/16 v2, 0x3c
aput-byte v2, v0, v1
const/16 v1, 0xf
const/16 v2, 0xf
aput-byte v2, v0, v1
sput-object v0, Lapkil/tests/APKIL;->KEY:[B
new-array v0, v7, [B
const/4 v1, 0x1
const/16 v2, 0x2a
aput-byte v2, v0, v1
aput-byte v3, v0, v3
const/4 v1, 0x3
const/16 v2, 0x36
aput-byte v2, v0, v1
aput-byte v4, v0, v4
const/4 v1, 0x5
const/16 v2, 0x2d
aput-byte v2, v0, v1
aput-byte v5, v0, v5
aput-byte v7, v0, v6
sput-object v0, Lapkil/tests/APKIL;->KEY2:[B
return-void
.end method
.method public constructor <init>()V
.registers 1
invoke-direct {p0}, Landroid/app/Activity;-><init>()V
return-void
.end method
.method private toHex([B)Ljava/lang/String;
.registers 7
new-instance v1, Ljava/lang/StringBuffer;
invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V
const/4 v2, 0x0
:goto_6
array-length v3, p1
if-lt v2, v3, :cond_e
invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
move-result-object v3
return-object v3
:cond_e
aget-byte v3, p1, v2
and-int/lit16 v3, v3, 0xff
invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;
move-result-object v0
:goto_16
invoke-virtual {v0}, Ljava/lang/String;->length()I
move-result v3
const/4 v4, 0x2
if-lt v3, v4, :cond_23
invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
add-int/lit8 v2, v2, 0x1
goto :goto_6
:cond_23
new-instance v3, Ljava/lang/StringBuilder;
const-string v4, "0"
invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v3
invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v0
goto :goto_16
.end method
.method public onCreate(Landroid/os/Bundle;)V
.registers 4
invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V
const/high16 v1, 0x7f03
invoke-virtual {p0, v1}, Lapkil/tests/APKIL;->setContentView(I)V
const-string v1, "phone"
invoke-virtual {p0, v1}, Lapkil/tests/APKIL;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;
move-result-object v0
check-cast v0, Landroid/telephony/TelephonyManager;
invoke-static {v0}, Ldroidbox/android/telephony/TelephonyManager;->getDeviceId(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lapkil/tests/APKIL;->imei:Ljava/lang/String;
invoke-static {v0}, Ldroidbox/android/telephony/TelephonyManager;->getSubscriberId(Landroid/telephony/TelephonyManager;)Ljava/lang/String;
move-result-object v1
iput-object v1, p0, Lapkil/tests/APKIL;->imsi:Ljava/lang/String;
invoke-virtual {p0}, Lapkil/tests/APKIL;->testCryptHash()V
invoke-virtual {p0}, Lapkil/tests/APKIL;->testCryptAES()V
invoke-virtual {p0}, Lapkil/tests/APKIL;->testCryptDES()V
invoke-virtual {p0}, Lapkil/tests/APKIL;->testWriteFile()V
invoke-virtual {p0}, Lapkil/tests/APKIL;->testReadFile()V
invoke-virtual {p0}, Lapkil/tests/APKIL;->testGetInstalledApps()V
invoke-virtual {p0}, Lapkil/tests/APKIL;->testSendSMS()V
invoke-virtual {p0}, Lapkil/tests/APKIL;->testPhoneCall()V
return-void
.end method
.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
.registers 4
invoke-virtual {p0}, Lapkil/tests/APKIL;->getMenuInflater()Landroid/view/MenuInflater;
move-result-object v0
const/high16 v1, 0x7f07
invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V
const/4 v0, 0x1
return v0
.end method
.method public testCryptAES()V
.registers 11
const-string v8, "APKILTest"
const-string v9, "[*] testCryptAES()"
invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:try_start_7
const-string v8, "AES"
invoke-static {v8}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v0
new-instance v7, Ljavax/crypto/spec/SecretKeySpec;
sget-object v8, Lapkil/tests/APKIL;->KEY:[B
const-string v9, "AES"
invoke-static {v8, v9}, Ldroidbox/javax/crypto/spec/SecretKeySpec;->droidbox_cons([BLjava/lang/String;)V
invoke-direct {v7, v8, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
const/4 v8, 0x1
invoke-static {v0, v8, v7}, Ldroidbox/javax/crypto/Cipher;->init(Ljavax/crypto/Cipher;ILjava/security/Key;)V
iget-object v8, p0, Lapkil/tests/APKIL;->imei:Ljava/lang/String;
invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B
move-result-object v3
invoke-static {v0, v3}, Ldroidbox/javax/crypto/Cipher;->doFinal(Ljavax/crypto/Cipher;[B)[B
move-result-object v5
invoke-direct {p0, v5}, Lapkil/tests/APKIL;->toHex([B)Ljava/lang/String;
move-result-object v6
const-string v8, "AES"
invoke-static {v8}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v1
new-instance v2, Ljavax/crypto/spec/SecretKeySpec;
sget-object v8, Lapkil/tests/APKIL;->KEY:[B
const-string v9, "AES"
invoke-static {v8, v9}, Ldroidbox/javax/crypto/spec/SecretKeySpec;->droidbox_cons([BLjava/lang/String;)V
invoke-direct {v2, v8, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
const/4 v8, 0x2
invoke-static {v1, v8, v2}, Ldroidbox/javax/crypto/Cipher;->init(Ljavax/crypto/Cipher;ILjava/security/Key;)V
invoke-static {v1, v5}, Ldroidbox/javax/crypto/Cipher;->doFinal(Ljavax/crypto/Cipher;[B)[B
:try_end_3e
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_7 .. :try_end_3e} :catch_3f
.catch Ljavax/crypto/NoSuchPaddingException; {:try_start_7 .. :try_end_3e} :catch_44
.catch Ljava/security/InvalidKeyException; {:try_start_7 .. :try_end_3e} :catch_49
.catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_7 .. :try_end_3e} :catch_4e
.catch Ljavax/crypto/BadPaddingException; {:try_start_7 .. :try_end_3e} :catch_53
:goto_3e
return-void
:catch_3f
move-exception v4
invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
goto :goto_3e
:catch_44
move-exception v4
invoke-virtual {v4}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V
goto :goto_3e
:catch_49
move-exception v4
invoke-virtual {v4}, Ljava/security/InvalidKeyException;->printStackTrace()V
goto :goto_3e
:catch_4e
move-exception v4
invoke-virtual {v4}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V
goto :goto_3e
:catch_53
move-exception v4
invoke-virtual {v4}, Ljavax/crypto/BadPaddingException;->printStackTrace()V
goto :goto_3e
.end method
.method public testCryptDES()V
.registers 11
const-string v8, "APKILTest"
const-string v9, "[*] testCryptDES()"
invoke-static {v8, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:try_start_7
const-string v8, "DES"
invoke-static {v8}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v0
new-instance v7, Ljavax/crypto/spec/SecretKeySpec;
sget-object v8, Lapkil/tests/APKIL;->KEY2:[B
const-string v9, "DES"
invoke-static {v8, v9}, Ldroidbox/javax/crypto/spec/SecretKeySpec;->droidbox_cons([BLjava/lang/String;)V
invoke-direct {v7, v8, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
const/4 v8, 0x1
invoke-static {v0, v8, v7}, Ldroidbox/javax/crypto/Cipher;->init(Ljavax/crypto/Cipher;ILjava/security/Key;)V
iget-object v8, p0, Lapkil/tests/APKIL;->imei:Ljava/lang/String;
invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B
move-result-object v3
invoke-static {v0, v3}, Ldroidbox/javax/crypto/Cipher;->doFinal(Ljavax/crypto/Cipher;[B)[B
move-result-object v5
invoke-direct {p0, v5}, Lapkil/tests/APKIL;->toHex([B)Ljava/lang/String;
move-result-object v6
const-string v8, "DES"
invoke-static {v8}, Ldroidbox/javax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;
move-result-object v1
new-instance v2, Ljavax/crypto/spec/SecretKeySpec;
sget-object v8, Lapkil/tests/APKIL;->KEY2:[B
const-string v9, "DES"
invoke-static {v8, v9}, Ldroidbox/javax/crypto/spec/SecretKeySpec;->droidbox_cons([BLjava/lang/String;)V
invoke-direct {v2, v8, v9}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V
const/4 v8, 0x2
invoke-static {v1, v8, v2}, Ldroidbox/javax/crypto/Cipher;->init(Ljavax/crypto/Cipher;ILjava/security/Key;)V
invoke-static {v1, v5}, Ldroidbox/javax/crypto/Cipher;->doFinal(Ljavax/crypto/Cipher;[B)[B
:try_end_3e
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_7 .. :try_end_3e} :catch_3f
.catch Ljavax/crypto/NoSuchPaddingException; {:try_start_7 .. :try_end_3e} :catch_44
.catch Ljava/security/InvalidKeyException; {:try_start_7 .. :try_end_3e} :catch_49
.catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_7 .. :try_end_3e} :catch_4e
.catch Ljavax/crypto/BadPaddingException; {:try_start_7 .. :try_end_3e} :catch_53
:goto_3e
return-void
:catch_3f
move-exception v4
invoke-virtual {v4}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
goto :goto_3e
:catch_44
move-exception v4
invoke-virtual {v4}, Ljavax/crypto/NoSuchPaddingException;->printStackTrace()V
goto :goto_3e
:catch_49
move-exception v4
invoke-virtual {v4}, Ljava/security/InvalidKeyException;->printStackTrace()V
goto :goto_3e
:catch_4e
move-exception v4
invoke-virtual {v4}, Ljavax/crypto/IllegalBlockSizeException;->printStackTrace()V
goto :goto_3e
:catch_53
move-exception v4
invoke-virtual {v4}, Ljavax/crypto/BadPaddingException;->printStackTrace()V
goto :goto_3e
.end method
.method public testCryptHash()V
.registers 8
const-string v5, "APKILTest"
const-string v6, "[*] testCryptHash()"
invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
const-string v4, "Hash me"
const/4 v0, 0x0
:try_start_a
const-string v5, "MD5"
invoke-static {v5}, Ldroidbox/java/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
move-result-object v0
invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B
move-result-object v5
invoke-static {v0, v5}, Ldroidbox/java/security/MessageDigest;->update(Ljava/security/MessageDigest;[B)V
invoke-static {v0}, Ldroidbox/java/security/MessageDigest;->digest(Ljava/security/MessageDigest;)[B
move-result-object v3
invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B
move-result-object v5
invoke-static {v0, v5}, Ldroidbox/java/security/MessageDigest;->digest(Ljava/security/MessageDigest;[B)[B
const-string v5, "SHA1"
invoke-static {v5}, Ldroidbox/java/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
move-result-object v0
invoke-virtual {v4}, Ljava/lang/String;->getBytes()[B
move-result-object v5
invoke-static {v0, v5}, Ldroidbox/java/security/MessageDigest;->update(Ljava/security/MessageDigest;[B)V
invoke-static {v0}, Ldroidbox/java/security/MessageDigest;->digest(Ljava/security/MessageDigest;)[B
move-result-object v3
const/4 v0, 0x0
const-string v5, "SHA1"
invoke-static {v5}, Ldroidbox/java/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
move-result-object v0
iget-object v5, p0, Lapkil/tests/APKIL;->imei:Ljava/lang/String;
invoke-virtual {v5}, Ljava/lang/String;->getBytes()[B
move-result-object v5
invoke-static {v0, v5}, Ldroidbox/java/security/MessageDigest;->update(Ljava/security/MessageDigest;[B)V
invoke-static {v0}, Ldroidbox/java/security/MessageDigest;->digest(Ljava/security/MessageDigest;)[B
move-result-object v3
invoke-direct {p0, v3}, Lapkil/tests/APKIL;->toHex([B)Ljava/lang/String;
:try_end_4a
.catch Ljava/security/NoSuchAlgorithmException; {:try_start_a .. :try_end_4a} :catch_4c
move-result-object v2
:goto_4b
return-void
:catch_4c
move-exception v1
invoke-virtual {v1}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V
goto :goto_4b
.end method
.method public testGetInstalledApps()V
.registers 5
const-string v2, "Test"
const-string v3, "[*] testGetInstalledApps()"
invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {p0}, Lapkil/tests/APKIL;->getPackageManager()Landroid/content/pm/PackageManager;
move-result-object v1
const/16 v2, 0x80
invoke-static {v1, v2}, Ldroidbox/android/content/pm/PackageManager;->getInstalledApplications(Landroid/content/pm/PackageManager;I)Ljava/util/List;
move-result-object v0
return-void
.end method
.method public testNetworkHTTP()V
.registers 10
const-string v7, "APKILTest"
const-string v8, "[*] testNetworkHTTP()"
invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
const/4 v4, 0x0
const/4 v6, 0x0
:try_start_9
new-instance v5, Ljava/net/URL;
const-string v7, "http://www.seckungfu.com"
invoke-static {v7}, Ldroidbox/java/net/URL;->droidbox_cons(Ljava/lang/String;)V
invoke-direct {v5, v7}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
:try_start_10
:try_end_10
.catchall {:try_start_9 .. :try_end_10} :catchall_42
.catch Ljava/io/IOException; {:try_start_9 .. :try_end_10} :catch_3a
const-string v7, "APKILTest"
invoke-virtual {v5}, Ljava/net/URL;->toString()Ljava/lang/String;
move-result-object v8
invoke-static {v7, v8}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
invoke-static {v5}, Ldroidbox/java/net/URL;->openConnection(Ljava/net/URL;)Ljava/net/URLConnection;
move-result-object v7
move-object v0, v7
check-cast v0, Ljava/net/HttpURLConnection;
move-object v6, v0
new-instance v3, Ljava/io/BufferedReader;
new-instance v7, Ljava/io/InputStreamReader;
invoke-virtual {v6}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
move-result-object v8
invoke-direct {v7, v8}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
invoke-direct {v3, v7}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
:cond_2f
invoke-static {v3}, Ldroidbox/java/io/BufferedReader;->readLine(Ljava/io/BufferedReader;)Ljava/lang/String;
:try_end_32
.catchall {:try_start_10 .. :try_end_32} :catchall_47
.catch Ljava/io/IOException; {:try_start_10 .. :try_end_32} :catch_4a
move-result-object v2
if-nez v2, :cond_2f
invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
move-object v4, v5
:goto_39
return-void
:catch_3a
move-exception v1
:try_start_3b
:goto_3b
invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
:try_end_3e
.catchall {:try_start_3b .. :try_end_3e} :catchall_42
invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
goto :goto_39
:catchall_42
move-exception v7
:goto_43
invoke-virtual {v6}, Ljava/net/HttpURLConnection;->disconnect()V
throw v7
:catchall_47
move-exception v7
move-object v4, v5
goto :goto_43
:catch_4a
move-exception v1
move-object v4, v5
goto :goto_3b
.end method
.method public testPhoneCall()V
.registers 4
const-string v1, "APKILTest"
const-string v2, "[*] testPhoneCall()"
invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
new-instance v0, Landroid/content/Intent;
const-string v1, "android.intent.action.CALL"
invoke-static {v1}, Ldroidbox/android/content/Intent;->droidbox_cons(Ljava/lang/String;)V
invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V
const-string v1, "tel:10010"
invoke-static {v1}, Ldroidbox/android/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
move-result-object v1
invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;
invoke-static {p0, v0}, Ldroidbox/apkil/tests/APKIL;->startActivity(Lapkil/tests/APKIL;Landroid/content/Intent;)V
return-void
.end method
.method public testReadFile()V
.registers 9
const-string v5, "APKILTest"
const-string v6, "[*] testReadFile()"
invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:try_start_7
const-string v5, "myfilename.txt"
invoke-static {p0, v5}, Ldroidbox/apkil/tests/APKIL;->openFileInput(Lapkil/tests/APKIL;Ljava/lang/String;)Ljava/io/FileInputStream;
move-result-object v3
if-eqz v3, :cond_1f
new-instance v2, Ljava/io/InputStreamReader;
invoke-direct {v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V
new-instance v0, Ljava/io/BufferedReader;
invoke-direct {v0, v2}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V
:goto_19
invoke-static {v0}, Ldroidbox/java/io/BufferedReader;->readLine(Ljava/io/BufferedReader;)Ljava/lang/String;
move-result-object v4
if-nez v4, :cond_45
:cond_1f
const-string v5, "FileContent"
new-instance v6, Ljava/lang/StringBuilder;
iget-object v7, p0, Lapkil/tests/APKIL;->fileContent:Ljava/lang/String;
invoke-virtual {v7}, Ljava/lang/String;->length()I
move-result v7
invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;
move-result-object v7
invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v7
invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
iget-object v7, p0, Lapkil/tests/APKIL;->fileContent:Ljava/lang/String;
invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v6
invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v6
invoke-static {v5, v6}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
invoke-virtual {v3}, Ljava/io/InputStream;->close()V
:goto_44
return-void
:cond_45
iget-object v5, p0, Lapkil/tests/APKIL;->fileContent:Ljava/lang/String;
new-instance v6, Ljava/lang/StringBuilder;
invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;
move-result-object v5
invoke-direct {v6, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
move-result-object v5
invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
move-result-object v5
iput-object v5, p0, Lapkil/tests/APKIL;->fileContent:Ljava/lang/String;
:try_end_5a
.catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_5a} :catch_5b
.catch Ljava/io/IOException; {:try_start_7 .. :try_end_5a} :catch_60
goto :goto_19
:catch_5b
move-exception v1
invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V
goto :goto_44
:catch_60
move-exception v1
invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
goto :goto_44
.end method
.method public testSendSMS()V
.registers 7
const/4 v2, 0x0
const-string v1, "APKILTest"
const-string v3, "[*] testSendSMS()"
invoke-static {v1, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
invoke-static {}, Landroid/telephony/SmsManager;->getDefault()Landroid/telephony/SmsManager;
move-result-object v0
const-string v1, "+8601010010"
const-string v3, "Sending sms..."
move-object v4, v2
move-object v5, v2
invoke-static/range {v0 .. v5}, Ldroidbox/android/telephony/SmsManager;->sendTextMessage(Landroid/telephony/SmsManager;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)V
return-void
.end method
.method public testWriteFile()V
.registers 5
const-string v2, "APKILTest"
const-string v3, "[*] testWriteFile()"
invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:try_start_7
new-instance v1, Ljava/io/OutputStreamWriter;
const-string v2, "myfilename.txt"
const/4 v3, 0x0
invoke-static {p0, v2, v3}, Ldroidbox/apkil/tests/APKIL;->openFileOutput(Lapkil/tests/APKIL;Ljava/lang/String;I)Ljava/io/FileOutputStream;
move-result-object v2
invoke-direct {v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
const-string v2, "Write a line\n"
invoke-static {v1, v2}, Ldroidbox/java/io/OutputStreamWriter;->write(Ljava/io/OutputStreamWriter;Ljava/lang/String;)V
invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
:goto_1b
:try_end_1b
.catch Ljava/io/IOException; {:try_start_7 .. :try_end_1b} :catch_1c
return-void
:catch_1c
move-exception v0
invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
goto :goto_1b
.end method
.method public ttestWriteFile()V
.registers 5
const-string v2, "APKILTest"
const-string v3, "[*] testWriteFile()"
invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
:try_start_7
new-instance v1, Ljava/io/OutputStreamWriter;
const-string v2, "myfilename.txt"
const/4 v3, 0x0
invoke-static {p0, v2, v3}, Ldroidbox/apkil/tests/APKIL;->openFileOutput(Lapkil/tests/APKIL;Ljava/lang/String;I)Ljava/io/FileOutputStream;
move-result-object v2
invoke-direct {v1, v2}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;)V
const-string v2, "Write a line\n"
invoke-static {v1, v2}, Ldroidbox/java/io/OutputStreamWriter;->write(Ljava/io/OutputStreamWriter;Ljava/lang/String;)V
invoke-virtual {v1}, Ljava/io/OutputStreamWriter;->close()V
:goto_1b
:try_end_1b
.catch Ljava/io/IOException; {:try_start_7 .. :try_end_1b} :catch_1c
return-void
:catch_1c
move-exception v0
invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
goto :goto_1b
.end method