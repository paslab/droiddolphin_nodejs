#!/bin/bash
if [ ! -f "$2.log" ]
then
  APKNAME=`./scripts/andropkgname.py -i $2`
  ./APIMonitor-beta/apimonitor.py $2
  adb -s emulator-55$1 install $2.new
  adb -s emulator-55$1 logcat | grep "DroidBox" > $2.tmp &
  PID=$!
  adb -s emulator-55$1 shell monkey -p $APKNAME -v 1000
  sleep 5
  kill $PID
  adb -s emulator-55$1 uninstall $APKNAME
  mv $2.tmp $2.log
  python2 ~/feature.extract.py $3 $2.log > $2.log.svm
  rm $2.new
else
  echo "$2.log exists, Skip..."
fi

if [ ! -f "$2.log.svm" ]
then
  python2 ~/feature.extract.py $3 $2.log > $2.log.svm
else
  echo "$2.log.svm exists, Skip..."
fi
