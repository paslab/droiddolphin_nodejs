#!/bin/bash
rm $2.log
START=$(date +%s.%N)
for apk in `ls $1/*.apk`
do
  echo $apk >> $2.log
  ./apimonitor.sh $2 $apk $3
  END=$(date +%s.%N)
  DIFF=$(echo "$END-$START" | bc)
  echo $DIFF >> run.log
done
