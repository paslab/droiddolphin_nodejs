#!/bin/bash

base_dir=$(dirname $0)

for no in $(seq -w 00 08)
do
  bash ${base_dir}/startemu.sh A$no
  sleep 30
done
