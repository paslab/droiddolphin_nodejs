#!/bin/bash

base_dir=$(dirname $0)
upload_dir="${base_dir}/../uploads"
record_dir="${base_dir}/../record"
svm_predict="${base_dir}/../libsvm/svm-predict"
num_log=1000

main() {
  while [ 1 ]
  do
    apk=$(ls $upload_dir | head -n 1)
    new_apk=$(basename "$apk")
    new_apk="${new_apk%.*}.new.apk"
    if [ "${apk}" = "" ]; then
      #mongo droiddolphin < ./remove.js >> /dev/null
      #date
      #echo "Nothing to do, go sleeping..."
      sleep 10
    else
      pkgname=$(${base_dir}/androguard/andropkgname.py -i ${upload_dir}/${apk})
      sha1=$(sha1sum ${upload_dir}/${apk} | cut -d ' ' -f1)
      date=${apk%.apk}
      search=$(cat malware.lst | grep ${sha1})
      if [ "${search}" = "" ]; then
        printf "Not in malware Database\n"
      else
        #node ./update.js ${date} ${sha1} 1 Done
        rm -f ${upload_dir}/${apk}
        continue
      fi
      #node ./update.js ${date} ${sha1} Unknown Running
      ${base_dir}/analyzer/APIMonitor-beta/apimonitor.py ${upload_dir}/${apk}
      adb install ${upload_dir}/${new_apk}
      adb logcat | grep "DroidBox" > ${record_dir}/log/${apk}.log &
      PID=$!
      adb shell monkey -p ${pkgname} -v ${num_log}
      sleep 5
      kill $PID 2>&1 > /dev/null
      adb uninstall ${pkgname}
      ${base_dir}/feature.extract.py 0 ${record_dir}/log/${apk}.log > ${record_dir}/svm/${apk}.svm
      $svm_predict ${record_dir}/svm/${apk}.svm ${base_dir}/svm.model ${record_dir}/out/${apk}.out
      res="$(cat ${record_dir}/out/${apk}.out)"
      node ${base_dir}/update.js ${date} ${sha1} ${res} Done
      rm -rf ${upload_dir}/${apk} ${upload_dir}/apimonitor_out ${upload_dir}/${new_apk}
      now=$(date)
      printf "${now}: ${apk}\n"
    fi
  done
}

main "$@"
