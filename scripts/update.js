var mongoose_util = require('../mongoose_util.js');
mongoose_util.init();

var upload_date = process.argv[2];
var sha1 = process.argv[3];
var classification = process.argv[4];
var status = process.argv[5];

mongoose_util.update(mongoose_util.Record, {upload_date: upload_date}, {sha1: sha1, classification: classification, status: status}, {}, function(err, n_effected){
  mongoose_util.close();
});

