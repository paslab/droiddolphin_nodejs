var mongoose = require('mongoose');

exports.Record = null;

exports.init = function(){
  mongoose.connect('mongodb://localhost/droiddolphin');

  var db = mongoose.connection;
  db.on('error',console.error.bind(console,'connection error:'));
  db.once('open',function(){
    console.log("MongoDB connected");
  });

  //Schema
  this.Record = record_init();
}

exports.close = function(){
  mongoose.disconnect();
  console.log("MongoDB disconnected");
}

exports.save = function(obj,cb){
  obj.save(function(err, obj){
    if(err)
      console.log(err);
    cb();
  })
}

exports.update = function(model, cond, update, options, cb){
  model.update(cond, update, options, cb);
}

function record_init(){
  var record_schema = mongoose.Schema({
    upload_date:     {type: String,  default: ""},
    name:            {type: String,  default: ""},
    sha1:            {type: String,  default: ""},
    description:     {type: String,  default: ""},
    source:          {type: String,  default: ""},
    is_malware:      {type: String,  default: ""},
    classification:  {type: String,  default: ""},
    status:          {type: String,  default: "Queuing"}
  });

  record_schema.methods.log = function(){
    console.log(this);
  }

  return mongoose.model('Record',record_schema);
}
